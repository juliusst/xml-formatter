package lt.js.xmlformatter.service;

public abstract class AgreementXmlForm<T> {

	public abstract String generatePaymentXml(T argTo);
	
	public abstract String generateReportXml(T argTo);

	protected String buildXmlString() {
		return null; //TODO add implementation
	}
   	
}
