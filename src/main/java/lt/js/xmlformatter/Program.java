package lt.js.xmlformatter;

import lt.js.xmlformatter.bean.OffboardingXmlArgTo;
import lt.js.xmlformatter.bean.OnboardingXmlArgTo;
import lt.js.xmlformatter.service.AgreementOffboardingXmlServiceFactory;
import lt.js.xmlformatter.service.AgreementOnboardingXmlServiceFactory;

public class Program {

	public static void main(String[] args) {
		AgreementOffboardingXmlServiceFactory offboardingXmlService = new AgreementOffboardingXmlServiceFactory();
		OffboardingXmlArgTo offboardingXmlArgTo = new OffboardingXmlArgTo("paymentno", "reportno");
		final String OffboardingPayment = offboardingXmlService.generatePaymentXml(offboardingXmlArgTo);
		final String OffboardingReport = offboardingXmlService.generateReportXml(offboardingXmlArgTo);
        
		
		/////////////////////////////////////////////// 
		
		AgreementOnboardingXmlServiceFactory onboardingXmlService = new AgreementOnboardingXmlServiceFactory();
		OnboardingXmlArgTo onboardingXmlArgTo = new OnboardingXmlArgTo("paymentno2", "reportno2", "reportno2",
				"reportno2", "reportno2", "reportno2");
		final String payment = onboardingXmlService.generatePaymentXml(onboardingXmlArgTo);
		final String report = onboardingXmlService.generateReportXml(onboardingXmlArgTo);
	}

}
