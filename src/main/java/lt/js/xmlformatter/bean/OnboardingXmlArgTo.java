package lt.js.xmlformatter.bean;

import java.io.Serializable;

public class OnboardingXmlArgTo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String paymentAgreementNo;
	
	private String reportAgreementNo;
	
	private String reportAgreementNo1;
	
	private String reportCountry;
	
	private String reportCurrency;
	
	private String reportCurrency2;

	public OnboardingXmlArgTo(String paymentAgreementNo, String reportAgreementNo, String reportAgreementNo1,
			String reportCountry, String reportCurrency, String reportCurrency2) {
		this.paymentAgreementNo = paymentAgreementNo;
		this.reportAgreementNo = reportAgreementNo;
		this.reportAgreementNo1 = reportAgreementNo1;
		this.reportCountry = reportCountry;
		this.reportCurrency = reportCurrency;
		this.reportCurrency2 = reportCurrency2;
	}

	public String getPaymentAgreementNo() {
		return paymentAgreementNo;
	}

	public String getReportAgreementNo() {
		return reportAgreementNo;
	}

	public String getReportAgreementNo1() {
		return reportAgreementNo1;
	}

	public String getReportCountry() {
		return reportCountry;
	}

	public String getReportCurrency() {
		return reportCurrency;
	}

	public String getReportCurrency2() {
		return reportCurrency2;
	}

}
