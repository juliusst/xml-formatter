package lt.js.xmlformatter.bean;

import java.io.Serializable;

public class OffboardingXmlArgTo implements Serializable {

	private static final long serialVersionUID = 1L;

    private String paymentAgreementNo;
    
    private String reportAgreementNo;

	public OffboardingXmlArgTo(String paymentAgreementNo, String reportAgreementNo) {
		this.paymentAgreementNo = paymentAgreementNo;
		this.reportAgreementNo = reportAgreementNo;
	}

	public String getPaymentAgreementNo() {
		return paymentAgreementNo;
	}

	public String getReportAgreementNo() {
		return reportAgreementNo;
	}
	
	
    
    

}
